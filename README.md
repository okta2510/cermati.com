# cermati.com_frontend_test

> oktaviardi pratama |  Cermati.com Front-end Developer Entry Test

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```
using NVM 12.3.1
or NPM 6.9.0
For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
